#!/usr/bin/env python3

import sys
from greek_normalisation.convert_files import convert
from greek_normalisation.utils import nfc, nfd, strip_accents

def strip_line(line):
    line = line.strip()
    line = strip_accents(line)
    line = nfd(line)
    # breathings
    line = line.replace('\u0313', '')
    line = line.replace('\u0314', '')
    line = nfc(line)
    return line + '\n'

convert(strip_line)
