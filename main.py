from enum import Enum, auto, unique
import random

from ansi.color import fg

from formatting import ask_with_answers, format_ref, format_verse
from parsing import PartOfSpeech, Case, Gender, Tense, Number, Person, Mood, Voice
from parsing import constrain_words, fetch_by_book, find_red_herrings
from utils import unique_by

@unique
class Question(Enum):
    LEXEME = auto()
    TENSE = auto()
    NUMBER = auto()
    PERSON = auto()
    CASE = auto()
    GENDER = auto()

words = fetch_by_book(4)
verses = {}
for word in words:
    if word['bcv'] not in verses:
        verses[word['bcv']] = []
    verses[word['bcv']].append(word)
forms = unique_by('norm', words)


def execute_question_block(question_block):
    constrained_forms = constrain_words(forms, question_block['constraints'])

    question_queue = question_block['questions'][:]
    random.shuffle(question_queue)
    while len(question_queue) < 10:
        question_queue = question_queue * 2
    question_queue = question_queue[0:10]

    for question in question_queue:
        question_forms = constrained_forms
        # TODO: this won't always work if we turn off the VERB global constraint;
        #       it needs an else block
        if 'constraints' in question:
            question_forms = constrain_words(question_forms, question['constraints'])

        question_form = random.choice(question_forms)

        form_instances = [w for w in words if w['norm'] == question_form['norm']]
        form_instance = random.choice(form_instances)
        verse = verses[form_instance['bcv']]

        if question['type'] == Question.LEXEME:
            message = 'What is the lexeme?'
            correct_answer = question_form['lemma']
            answers = [question_form['lemma'], question_form['norm']]
            answers += find_red_herrings(forms, answers, 5)
            answers = answers[0:5]
            answers = list(set(answers))
            random.shuffle(answers)
        elif question['type'] == Question.TENSE:
            message = 'What is the tense?'
            correct_answer = form_instance['parse'].tense
            answers = list(set([w['parse'].tense for w in constrained_forms]))
            if None in answers: answers.remove(None)
            answers.sort(key=lambda w: w.name)
        elif question['type'] == Question.NUMBER:
            message = 'What is the number?'
            correct_answer = form_instance['parse'].number
            answers = list(Number)
        elif question['type'] == Question.PERSON:
            message = 'What is the person?'
            if form_instance['parse'].person == Person.FIRST:
                correct_answer = 'I / we'
            elif form_instance['parse'].person == Person.SECOND:
                correct_answer = 'you'
            else:
                correct_answer = 'he / she / it / them'
            answers = ['I / we', 'you', 'he / she / it / them']
        elif question['type'] == Question.CASE:
            message = 'What is the case?'
            correct_answer = form_instance['parse'].case
            answers = list(Case)
        elif question['type'] == Question.GENDER:
            message = 'What is the gender?'
            correct_answer = form_instance['parse'].gender
            answers = list(Gender)
        else:
            raise ValueError('Unknown question type: %s' % question['type'])


        print()
        print(format_verse(verse, form_instance['index']))
        print('  -- %s' % format_ref(form_instance['bcv']))
        print()

        message = print(message)
        result = ask_with_answers(message, answers)
        if not result:
            break

        if result == correct_answer:
            print(fg.green('Correct'))
        else:
            print(fg.red('Incorrect:'), fg.yellow(str(correct_answer)))


QUESTION_BLOCKS = [
    {
        'description': 'Present/Future Active Indicative Verbs',
        'constraints': [
            PartOfSpeech.VERB,
            Mood.INDICATIVE,
            Voice.ACTIVE,
            [Tense.PRESENT, Tense.FUTURE],
        ],
        'questions': [
            { 'type': Question.LEXEME },
            { 'type': Question.TENSE },
            { 'type': Question.NUMBER },
            { 'type': Question.PERSON },
        ]
    },
    {
        'description': 'Singular Masculine Nouns',
        'constraints': [
            PartOfSpeech.NOUN,
            Number.SINGULAR,
            Gender.MASCULINE,
        ],
        'questions': [
            { 'type': Question.LEXEME },
            { 'type': Question.CASE },
        ]
    },
    {
        'description': 'Conjunctions',
        'constraints': [
            PartOfSpeech.CONJUNCTION,
        ],
        'questions': [
            { 'type': Question.LEXEME },
        ]
    },
    {
        'description': 'Singular Masculine The',
        'constraints': [
            PartOfSpeech.DEFINITE_ARTICLE,
            Number.SINGULAR,
            Gender.MASCULINE,
        ],
        'questions': [
            { 'type': Question.LEXEME },
            { 'type': Question.CASE },
        ]
    },
    {
        'description': 'Plural Masculine The',
        'constraints': [
            PartOfSpeech.DEFINITE_ARTICLE,
            Number.PLURAL,
            Gender.MASCULINE,
        ],
        'questions': [
            { 'type': Question.LEXEME },
            { 'type': Question.CASE },
        ]
    },
]


block_names = [b['description'] for b in QUESTION_BLOCKS]
while True:
    chosen_block_name = ask_with_answers('Choose a question block:', block_names)
    if chosen_block_name is None:
        break

    chosen_block = [b for b in QUESTION_BLOCKS if b['description'] == chosen_block_name][0]

    execute_question_block(chosen_block)

    # TODO: declension filters
    # TODO: record result for the word's attributes going into each question
    # TODO: compute coverage stats across attributes joined with each other
    # TODO: dim words that don't have high parsing likelihood
    # TODO: balance questions (or weight towards low coverage)
    # TODO: balance other attributes not currently being tested
    # TODO: calculate likelihood of parsing particular words, and find
    #       verse/passages that are likely in range of parsing ability
    # TODO: a manually typed LEXEME question
