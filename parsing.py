from enum import Enum, auto, unique

from pylev3 import Levenshtein
from pysblgnt import morphgnt_rows

from utils import any_fn, all_fn, unique_by

@unique
class PartOfSpeech(Enum):
    ADJECTIVE = 'A-'
    CONJUNCTION = 'C-'
    ADVERB = 'D-'
    INTERJECTION = 'I-'
    NOUN = 'N-'
    PREPOSITION = 'P-'
    DEFINITE_ARTICLE = 'RA'
    DEMONSTRATIVE_PRONOUN = 'RD'
    INTERROGATIVE_INDEFINITE_PRONOUN = 'RI'
    PERSONAL_PRONOUN = 'RP'
    RELATIVE_PRONOUN = 'RR'
    VERB = 'V-'
    PARTICLE = 'X-'
    def __str__(self): return self.name.lower().replace('_', ' ')

class Parse:
    def __init__(self, parse):
        self.person = None if parse[0] == '-' else Person(parse[0])
        self.tense = None if parse[1] == '-' else Tense(parse[1])
        self.voice = None if parse[2] == '-' else Voice(parse[2])
        self.mood = None if parse[3] == '-' else Mood(parse[3])
        self.case = None if parse[4] == '-' else Case(parse[4])
        self.number = None if parse[5] == '-' else Number(parse[5])
        self.gender = None if parse[6] == '-' else Gender(parse[6])
        self.degree = None if parse[7] == '-' else Degree(parse[7])
        self.attributes = [
                self.person, self.tense,
                self.voice, self.mood,
                self.case, self.number,
                self.gender, self.degree]
        self.attributes = [a for a in self.attributes if a is not None]
        self.attribute_set = set(self.attributes)

    def has(self, *properties):
        return self.attribute_set.issuperset(properties)

    def dense(self):
        def map(attr):
            return attr.value
        return ''.join(map(a) for a in self.attributes)

    def __str__(self):
        def map(attr):
            return {
                Person.FIRST: '1st',
                Person.SECOND: '2nd',
                Person.THIRD: '3rd',
            }.get(attr, str(attr))
        return ', '.join(map(a) for a in self.attributes)

    def __repr__(self):
        return '<' + ', '.join(str(a) for a in self.attributes) + '>'

@unique
class Person(Enum):
    FIRST = '1'
    SECOND = '2'
    THIRD = '3'
    def __str__(self): return self.name.lower()

@unique
class Tense(Enum):
    PRESENT = 'P'
    IMPERFECT = 'I'
    FUTURE = 'F'
    AORIST = 'A'
    PERFECT = 'X'
    PLUPERFECT = 'Y'
    def __str__(self): return self.name.lower()

@unique
class Voice(Enum):
    ACTIVE = 'A'
    MIDDLE = 'M'
    PASSIVE = 'P'
    def __str__(self): return self.name.lower()

@unique
class Mood(Enum):
    INDICATIVE = 'I'
    IMPERATIVE = 'D'
    SUBJUNCTIVE = 'S'
    OPTATIVE = 'O'
    INFINITIVE = 'N'
    PARTICIPLE = 'P'
    def __str__(self): return self.name.lower()

@unique
class Case(Enum):
    NOMINATIVE = 'N'
    GENITIVE = 'G'
    DATIVE = 'D'
    ACCUSATIVE = 'A'
    VOCATIVE = 'V'
    def __str__(self): return self.name.lower()

@unique
class Number(Enum):
    SINGULAR = 'S'
    PLURAL = 'P'
    def __str__(self): return self.name.lower()

@unique
class Gender(Enum):
    MASCULINE = 'M'
    FEMININE = 'F'
    NEUTER = 'N'
    def __str__(self): return self.name.lower()

@unique
class Degree(Enum):
    COMPARATIVE = 'C'
    SUPERLATIVE = 'S'
    def __str__(self): return self.name.lower()


BOOK_CACHE = {}
def fetch_by_book(book_number):
    if book_number in BOOK_CACHE:
        return BOOK_CACHE[book_number]
    words = read_book(book_number)
    BOOK_CACHE[book_number] = words
    return words

def fetch_by_chapter(book_number, chapter):
    words = fetch_by_book(book_number)
    return [w for w in words if w['loc']['chapter'] == chapter]


def read_book(book_number):
    words = []
    for row in morphgnt_rows(book_number):
        row['loc'] = parse_bcv(row['bcv'])

        row['pos'] = PartOfSpeech(row['ccat-pos'])
        del row['ccat-pos']

        row['parse'] = Parse(row['ccat-parse'])
        del row['ccat-parse']

        del row['robinson']

        row['text'] = row['text'].replace('\u2e02', "<")
        row['text'] = row['text'].replace('\u2e03', ">")
        row['norm'] = row['norm'].replace('(', '')
        row['norm'] = row['norm'].replace(')', '')

        row['index'] = len(words)
        words.append(row)

    return words

BOOKS = [
    None,
    'Matthew',
    'Mark',
    'Luke',
    'John',
    'Acts',
    'Romans',
    '1 Corinthians',
    '2 Corinthians',
    'Galatians',
    'Ephesians',
    'Philippians',
    'Colossians',
    '1 Thessalonians',
    '2 Thessalonians',
    '1 Timothy',
    '2 Timothy',
    'Titus',
    'Philemon',
    'Hebrews',
    'James',
    '1 Peter',
    '2 Peter',
    '1 John',
    '2 John',
    '3 John',
    'Jude',
    'Revelation',
]
def parse_bcv(bcv):
    book = int(bcv[0:2])
    chapter = int(bcv[2:4])
    verse = int(bcv[4:6])

    return {'book': BOOKS[book],
            'chapter': chapter,
            'verse': verse}

def parse_book_name(name):
    for i, book in enumerate(BOOKS):
        if book == name:
            return i
    return None


def constrain_words(words, constraints):
    if not isinstance(constraints, list):
        constraints = [constraints]
    def filter(word):
        def match_constraint(constraint):
            if isinstance(constraint, list):
                return any_fn(constraint, match_constraint)
            else:
                if isinstance(constraint, PartOfSpeech):
                    return word['pos'] == constraint
                else:
                    return constraint in word['parse'].attributes
        return all_fn(constraints, match_constraint)
    return [w for w in words if filter(w)]

def find_red_herrings(words, matches, count):
    words = [w['norm'] for w in words]
    distance = Levenshtein.damerau
    def sortfn(word):
        distances = [distance(m, word) for m in matches]
        smallest = min(distances)
        if smallest == 0: return 1000
        else: return smallest
    words.sort(key=sortfn)
    return words[0:count]
