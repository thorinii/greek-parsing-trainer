#!/usr/bin/env python3

import html
import re
import sys
import textwrap
from parsing import PartOfSpeech, fetch_by_chapter, parse_book_name

def render_as_doc(book_name, chapter, words, include_verse_numbers=True):
    passage = render_passage(words, include_verse_numbers)
    passage = indent(passage)

    css = indent(CSS_BLOCK)

    header = textwrap.dedent(f'''
        <html>
        <head>
            <title>Greek Linear {book_name} {chapter}</title>
        </head>
        <body>
            <h1>{book_name} {chapter}</h1>''').strip()
    footer = textwrap.dedent(f'''
        </body>
        </html>''').strip()
    return header + '\n' + css + '\n' + passage + '\n' + footer


def render_passage(words, include_verse_numbers=True):
    rendered = '<div class="text">\n'

    last_bcv = None
    for word in words:
        if last_bcv != word['bcv'] and include_verse_numbers:
            last_bcv = word['bcv']
            rendered += indent(render_verse_number(word['loc'])) + '\n'
        rendered += indent(render_word(word)) + '\n'
    rendered += '</div>'
    return rendered


def render_word(word):
    text = html.escape(word['text'])
    lemma = html.escape(word['lemma'])

    pos = {
        PartOfSpeech.ADJECTIVE: 'adj',
        PartOfSpeech.ADVERB: 'adv',
        PartOfSpeech.CONJUNCTION: 'conj',
        PartOfSpeech.DEFINITE_ARTICLE: 'art',
        PartOfSpeech.DEMONSTRATIVE_PRONOUN: 'pron',
        PartOfSpeech.INTERJECTION: 'interj',
        PartOfSpeech.INTERROGATIVE_INDEFINITE_PRONOUN: 'pron',
        PartOfSpeech.NOUN: 'noun',
        PartOfSpeech.PARTICLE: 'part',
        PartOfSpeech.PERSONAL_PRONOUN: 'pron',
        PartOfSpeech.PREPOSITION: 'prep',
        PartOfSpeech.RELATIVE_PRONOUN: 'pron',
        PartOfSpeech.VERB: 'verb',
    }.get(word['pos'], str(word['pos']))
    pos_long = str(word['pos'])

    parse = word['parse'].dense()
    parse_long = str(word['parse'])

    return textwrap.dedent(f'''
        <div class="word">
            <div class="word-text">{text}</div>
            <div class="word-lemma">{lemma}</div>
            <div class="word-pos" title="{pos_long}">{pos}</div>
            <div class="word-parse" title="{parse_long}">{parse}</div>
        </div>''').strip()


def render_verse_number(location):
    chapter = location['chapter']
    verse = location['verse']
    if verse == 1:
        return f'<div class="verse-number">{chapter}:{verse}</div>'
    else:
        return f'<div class="verse-number">{verse}</div>'


def indent(text):
    return textwrap.indent(text, '    ')


CSS_BLOCK = '''
<style>
body {
    margin: 0 auto;
    max-width: 70em;

    font-size: 16px;
}

h1 {
    margin: 1em 0;
}

.text {
    display: flex;
    flex-direction: row;
    flex-wrap: wrap;
    gap: 20px 0;

    font-size: 1.5em;
}

.verse-number {
    font-size: 0.6em;
    opacity: 0.6;
}
* + .verse-number {
    margin-left: 2em;
}

.word {
    margin: 0 0.5em;
    text-align: center;
}

.word .word-text {
}

.word .word-lemma,
.word .word-pos,
.word .word-parse {
    font-size: 0.6em;
    opacity: 0;
}
.word:hover .word-lemma,
.word:hover .word-pos,
.word:hover .word-parse {
    opacity: 1;
}
</style>'''.strip()


args = sys.argv[1:]
if len(args) == 0:
    print('Must specify a book and chapter', file=sys.stderr)
    sys.exit(1)

reference = ' '.join(args)
if re.match('^(([0-9] )?[a-zA-Z]+)$', reference):
    reference += ' 1'

split = re.match('^(([0-9] )?[a-zA-Z]+) ([0-9]+)$', reference)
book, chapter = split.group(1), int(split.group(3))
book_number = parse_book_name(book)
print(f'Rendering {book} {chapter}', file=sys.stderr)

words = fetch_by_chapter(book_number, chapter)
print(render_as_doc(book, chapter, words))
