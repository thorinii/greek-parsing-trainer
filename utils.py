def any_fn(list, fn):
    for item in list:
        if fn(item):
            return True
    return False

def all_fn(list, fn):
    for item in list:
        if not fn(item):
            return False
    return True

def unique_by(key, seq):
    unique = {}
    for item in seq:
        unique[item[key]] = item
    return list(unique.values())
