#!/usr/bin/env python3

import sys
from greek_normalisation.convert_files import convert
from greek_normalisation.normalise import Normaliser, Norm
from greek_normalisation.utils import nfc

normalise = Normaliser(config=(
    Norm.GRAVE |
    Norm.ELISION |
    Norm.MOVABLE |
    Norm.EXTRA |
    # don't convert everything to lowercase:
    # Norm.CAPITALISED |
    Norm.PROCLITIC |
    Norm.ENCLITIC |
    Norm.NO_ACCENT
)).normalise

def normalise_line(line):
    line = line.strip()
    if not line:
        return line + '\n'
    columns = line.split('\t')

    columns = [normalise_column(c) for c in columns]

    line = '\t'.join(columns)
    line = nfc(line)
    return line + '\n'

def normalise_column(column):
    pieces = column.split(' ')

    pieces = [normalise_text(p) for p in pieces]

    return ' '.join(pieces)

def normalise_text(text):
    result, _ = normalise(text)
    return result

convert(normalise_line)
