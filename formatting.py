from textwrap import TextWrapper

from ansi.color import fg
from PyInquirer import prompt

from utils import all_fn

def format_verse(words, highlighted_index):
    formatted_words = []
    for word in words:
        text = word['text']
        if word['index'] == highlighted_index:
            text = fg.green(text)
        formatted_words.append(text)

    text = ' '.join(formatted_words)
    wrapper = TextWrapper()
    wrapper.width = 70
    wrapper.initial_indent = '  '
    wrapper.subsequent_indent = '  '
    text = wrapper.fill(text)
    return text

BOOKS = [
    None,
    'Matthew',
    'Mark',
    'Luke',
    'John',
    'Acts',
    'Romans',
    '1 Corinthians',
    '2 Corinthians',
    'Galatians',
    'Ephesians',
    'Philippians',
    'Colossians',
    '1 Thessalonians',
    '2 Thessalonians',
    '1 Timothy',
    '2 Timothy',
    'Titus',
    'Philemon',
    'Hebrews',
    'James',
    '1 Peter',
    '2 Peter',
    '1 John',
    '2 John',
    '3 John',
    'Jude',
    'Revelation',
]
def format_ref(bcv):
    book = int(bcv[0:2])
    chapter = int(bcv[2:4])
    verse = int(bcv[4:6])
    return '%s %s:%s' % (BOOKS[book], chapter, verse)

def ask_with_answers(message, answers):
    is_enum = not all_fn(answers, lambda s: isinstance(s, str))

    strings = answers
    if is_enum:
        strings = [str(e) for e in strings]

    try:
        result = prompt({
            'type': 'list',
            'name': 'answer',
            'message': message,
            'choices': strings,
        })
    except EOFError:
        return None
    if not result:
        return None

    if is_enum:
        enum = type(answers[0])
        return enum[result['answer'].upper()]
    else:
        return result['answer']
